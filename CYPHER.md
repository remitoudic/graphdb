#   Neo4j / CYPHER introduction   ( WIP :blush: )
The first session was about graph theory and how to work with it and Python. This session is about  the graph DB NEO4J its query language Cypher. 


Cypher is graph query language graph.
The graph to quering are called property graph ( "The Labeled Property Graph Model" )

1. It contains nodes and relationships.
2. Nodes contain properties (key-value pairs).
3. Nodes can be labeled with one or more labels.
4. Relationships are named and directed, and always have a start and end node.
5. Relationships can also contain properties.


The language was designed with the power and capability of SQL (standard query language for the relational database model) in mind, but Cypher was based on the components and needs of a database built on the concepts of graph theory. In a graph model, data is structured as nodes and relationships (edges) to focus on how entities in the data are connected and related to one another.


##  Basic Syntax: 
The syntax is based on ASCII art. The language is  visual and easy to read because it both visually and structurally represents the data specified in the query. 

```
        - 1. Nodes are represented with parentheses  "(  )":
               ex: (variable:Label {propertyKey: 'propertyValue'})

        - 2. Relationship  are represented with square brackets  "[  ]":
               ex: -[variable:RELATIONSHIP_TYPE]->
               ex2:  -[variable:RELATIONSHIP_TYPE {propertyKey: 'propertyValue'}]->

        - 3. pattern:
              ex:  (node1:LabelA)-[rel1:RELATIONSHIP_TYPE]->(node2:LabelB)
                   (node1:LabelA) <-[rel1:RELATIONSHIP_TYPE]-(node2:LabelB)

```
## CRUD operations
Similar to other query languages, Cypher contains a variety of keywords for specifying CRUD operations.
In this section, the basic recovery unit will be taken as  an application. 

```
1. CREATE : 

// create a simple recovery unit: 
CREATE ( zero :node {aplication: 'zero', state: 'available'})
CREATE (one:node {aplication: 'one'})
CREATE (two:node {aplication: 'two'})

CREATE (zero)-[:START]->(one)
CREATE(one)-[:VERIFY]->(two)
CREATE (two)-[:STOP]->(zero)
CREATE (one)-[:STOP]->(zero)
```

```
2.READ  => "MATCH"

MATCH (n) RETURN n LIMIT 25
```

```
3. UPDATE  => "SET" 

// add  aproperty
MATCH (n {aplication: 'one' })
SET n.status = 'available'
RETURN n.aplication, n.status


// update a property
MATCH (n {aplication: 'one' })
SET n.status = 'unavailable'
RETURN n.aplication, n.status

```

```
4. DELETE   => "DETACH DELETE"

// Delete one node 
MATCH (n { aplication: 'one' })
DETACH DELETE n

// Delete all nodes
MATCH (n) DETACH DELETE n
```

## WORKSHOP  : 

```

1. Please got to  https://sandbox.neo4j.com/ and create a sandbox.
2. Use the  commandes  above to create your own recovery unit . 
3. Play with the other CRUD operations
 
```
## DEMO  Python with Neo4j 
```
The demo constists of doing the same but with using Python 

the code is available at : 

https://colab.research.google.com/drive/1lN4vfWZS7UheTD0GxEy3cmFDpIA2JUdz#scrollTo=iyuTfY2TrXwO

```